let Employee = require('../controllers/employee.controller');

module.exports = function(router) {
    router.post('/employee/create', Employee.createEmployee);
    router.post('/employee/calculate-credit/:id', Employee.calculateCredits);
    router.get('/employee/get', Employee.getEmployeeList);
    router.get('/employee/get/:id', Employee.getEmployee);
    router.put('/employee/update/:id', Employee.updateEmployee);
    router.delete('/employee/remove/:id', Employee.removeEmployee);
}