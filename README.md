# yes-doc-sample-nodejs

Nodejs API implememted with mongodb 

### Get Started

#### Requirements

- Node.js > 10.x
- MongoDB > 4

#### Launch 🚀

- `npm install -g migrate-mongo`
- `git clone https://yellhtet@bitbucket.org/yellhtet/yes-doc-sample-nodejs.git`
- `npm install`
- `migrate-mongo up`
- `npm start`

----

### Links

- [mongoose](https://mongoosejs.com/docs/guide.html)
- [Express](https://expressjs.com/en/4x/api.html)
- [MigrateMongo](https://github.com/seppevs/migrate-mongo)