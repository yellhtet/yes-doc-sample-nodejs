module.exports = {
  async up(db, client) {
    // const db = require("./config/database");
    const defaultEmployee = {
      empid: "EMP0001",
      name: "John Doe",
      address: "Yangon",
      dob: "12-12-1991",
      employerName: "Eric Chen",
      availableCredits: 100
    };
    await db.collection('employees').save(defaultEmployee);
  },

  async down(db, client) {
    // const db = require("./config/database");
    await db.collection('employees').remove({
      empid: {
        $eq: 'EMP0001'
      }
    });
  }
};