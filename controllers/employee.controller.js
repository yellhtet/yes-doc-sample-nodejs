let Employee = require("../DAO/employee.dao");

exports.createEmployee = function (req, res, next) {
  let emp = {
    empid: req.body.empid,
    name: req.body.name,
    address: req.body.address,
    dob: req.body.dob,
    employerName: req.body.employerName,
    availableCredits: req.body.availableCredits
  };

  Employee.create(emp, function (err, employee) {
    if (err) {
      res.json({
        error: err
      });
    }
    res.json({
      message: "Employee created successfully"
    });
  });
};

exports.calculateCredits = function (req, res) {
  let choosedCredit = req.body.choosedCredit;
  Employee.getById({
    empid: req.params.id
  }, function (err, employee) {
    if (err) {
      res.json({
        error: err
      });
    }
    if (employee) {
      let availableCredits = employee.availableCredits;
      let deductedCredit = Number(availableCredits) - Number(choosedCredit);
      if (deductedCredit < 0) {
        return res.json({
          status: 422,
          message: "You don't have enought credit to use."
        });
      }
      let updateData = {
        availableCredits: deductedCredit
      };
      Employee.update({
        empid: req.params.id
      }, updateData, function (
        err,
        employee
      ) {
        if (err) {
          res.json({
            error: err
          });
        }
        res.json({
          status: 200,
          message: "Employee credit updated successfully",
          data: employee
        });
      });
    } else {
      res.status(404).json({
        error: "Employee not found"
      });
    }
  });
};

exports.getEmployeeList = function (req, res, next) {
  Employee.get({}, function (err, employee) {
    if (err) {
      res.json({
        error: err
      });
    }
    res.json({
      employee: employee
    });
  });
};

exports.getEmployee = function (req, res, next) {
  Employee.getById({
    empid: req.params.id
  }, function (err, employee) {
    if (err) {
      res.json({
        error: err
      });
    }
    res.json({
      employee: employee
    });
  });
};

exports.updateEmployee = function (req, res, next) {
  let emp = {
    empid: req.body.empid,
    name: req.body.name,
    address: req.body.address,
    dob: req.body.dob,
    employerName: req.body.employerName
  };
  Employee.update({
    empid: req.params.empid
  }, emp, function (err, employee) {
    if (err) {
      res.json({
        error: err
      });
    }
    res.json({
      message: "Employee updated successfully"
    });
  });
};

exports.removeEmployee = function (req, res, next) {
  Employee.delete({
    empid: req.params.id
  }, function (err, employee) {
    if (err) {
      res.json({
        error: err
      });
    }
    res.json({
      message: "Employee deleted successfully"
    });
  });
};