const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let EmployeeSchema = new Schema({
  empid: { type: String, required: true },
  name: { type: String, required: true, max: 100 },
  address: { type: String, required: true },
  dob: { type: String, required: true },
  employerName: { type: String, required: true },
  availableCredits: { type: Number }
});

module.exports = EmployeeSchema;
